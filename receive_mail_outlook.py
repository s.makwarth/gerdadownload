"""
-------------------------------------------------------------------------------
Name:		receive_mail_outlook
Purpose:	Fetch download-ID from gerda and generate url for downloading gerda data
Author:   	Simon Makwarth, GKO-MST - simon dot makwarth at gmail dot com
Created:  	15-05-2019
Copyright: 	GKO-MST
License:	GNU Public License v3+
-------------------------------------------------------------------------------
"""
# ATTENTION: The script requires microsoft outlook 

import re
import win32com.client # pip install pywin32
import wget # pip install wget

def fetch_urls(account_name = r'njl_geofysik@mst.dk'):
	'''
	Return the urls to the gerda query download
	:param account_name: The name of the selected outlook inbox
	'''

	
	# Connect to Outlook, which has to be running and find the selected inbox
	outlook = win32com.client.Dispatch("Outlook.Application").GetNamespace("MAPI")
		
	for root_folder in outlook.Folders:		
		if root_folder.Name == account_name:
			break		
	
	inbox = root_folder.Folders('Indbakke')
	messages = inbox.Items
	print('Reading from "{}" in "{}" and the total messages is "{}"'.format(inbox.Name, root_folder.Name, len(messages)))
	
	# run through all mails inside mailbox and filter by subject regarding gerda data
	message = messages.GetFirst()
	urls = []
	methods = []
	select_messages = []
	i = 0
	for part in messages:
		if message.Subject.startswith('Geofysisk export job klar til download'):
			bodytext = message.Body
			
			# filter the download-ID from the mail and create url
			match = re.search(r'(exportstatus/)(\S+)',str(bodytext)) 
			id = match.group(2)
			url_gerda = str(r"http://gerda.geus.dk/Gerda/files/"+id+"/")
			urls.append(url_gerda)
			match_method = re.search(r"(p.data_type = ')(\w+)",str(bodytext))
			method = match_method.group(2)
			methods.append(method)
			select_messages.append(message)
			i += 1
		message=messages.GetNext()
	
	print('{} messages contained a gerda url'.format(i))
	return urls, methods, select_messages, root_folder
	outlook.close()

def download_data(urls, output_dir, name_file):
	'''
	Downloads data from collected urls 
	:param urls: a collection of url to download
	:param output_dir: directory to place dload files/
	:param name_file: name of dload file
	'''
	for j in range(0, len(urls)):
		print('')
		print('Downloading {}_DK.zip'.format(methods[j]))
		wget.download(str(urls[j]),'{}\{}_DK.zip'.format(output_dir, name_file[j]))

def move_messages(inbox_name, messages):
	'''
	Move selected mail to Indbakke/Imported
	:param inbox: the name of the outlook inbox
	:param messages: the array of messages to move
	'''

	for message in messages:
		message.move(inbox_name.Folders('Indbakke').Folders('Imported'))
	
# set output dir
output_dir = r'F:\GKO\data\grukos\gerda' # e.g. r'c:\gerda'

# run functions
urls, methods, select_messages, inbox = fetch_urls()
download_data(urls, output_dir, methods)
move_messages(inbox, select_messages)


		
		 
	