"""
-------------------------------------------------------------------------------
Name:      	GetGerdaData
Purpose:   	Initiate query for all geophysical data from GERDA database
Author:    	Simon Makwarth, simon dot makwarth at gmail dot com
Created:   	15-05-2019
Copyright: 	GKO-MST
License:	GNU Public License v3+
-------------------------------------------------------------------------------
"""
# ATTENTION: The script requires chromebrowser and chromedriver http://chromedriver.chromium.org/downloads
from time import sleep
from selenium import webdriver # pip install selenium, 

def open_gerda_url(url):
    '''connect to url in chrome and add query information.'''
		driver = webdriver.Chrome(executable_path=r"C:\Users\b028067\Chromedriver\chromedriver_win32\chromedriver.exe") 
	driver.get(url)
	
	#inset arg to boxes in browser
	driver.find_element_by_name('email').send_keys('njl_geofysik@mst.dk')
	driver.find_element_by_name('dbName').send_keys(datatype+'_DK')
	driver.find_element_by_name('dbFormat').send_keys("Firebird/Interbase")
	driver.find_element_by_name('datum').send_keys("euref89")
	# driver.find_element_by_name('includeRaw').click() # uncomment if raw data is desired
	# driver.find_element_by_name('includeOdvlacon').click() # uncomment if constraints for ODV is desired
	driver.find_element_by_name('checkLicense').click()
	driver.find_element_by_id('submtbtn').submit()

# set datatypes
datatypes = ['TEM', 'MEP', 'PACES', 'SKYTEM', 'LOG', 'SEISMIC'] # standard download
# datatypes = ['TEM', 'MEP', 'PACES', 'SCHLUMBERGER', 'SEISMIC', 'WENNER', 'SKYTEM', 'PACEP','LOG'] # full restore
# datatypes = ['WENNER','LOG'] # test
# datatypes = ['TEM'] # test 2


# for datatype in datatypes:
for datatype in datatypes:
	#Write url dataset is looped and polygon is fixed to span Denmark
	url = (
			r"http://gerda.geus.dk/Gerda/downloadPCGerda?sqlwhere=%20dsh.id%20IN%20%20(%20%20SELECT%20DISTINCT%20g.dataset_id%20FROM%20ger_dataset_and_models_spat_v%20g%20INNER%20JOIN%20ger%24project_spatial%20p%20ON%20g.proj_id%20%3D%20p.id%20AND%20g.data_type%20%3D%20p.data_type%20WHERE%201%3D1%20%0A%09%20and%20sdo_relate(%20g.geom%2C%20mdsys.sdo_geometry(%27POLYGON((426587.673611111%206433843.31597222%2C417928.8194444444%206002137.586805553%2C939934.0277777778%206019455.295138886%2C901587.673611111%206447450.086805553%2C426587.673611111%206433843.31597222))%27%2C25832)%2C%20%27mask%3Danyinteract%27)%20%3D%20%27TRUE%27%20%0A%09%20and%20sdo_relate(%20p.geom%2C%20mdsys.sdo_geometry(%27POLYGON((426587.673611111%206433843.31597222%2C417928.8194444444%206002137.586805553%2C939934.0277777778%206019455.295138886%2C901587.673611111%206447450.086805553%2C426587.673611111%206433843.31597222))%27%2C25832)%2C%20%27mask%3Danyinteract%27)%20%3D%20%27TRUE%27%20%0A%09%20and%20p.data_type%20%3D%20%27"
			+datatype+
			r"%27)%20"
			)
	open_gerda_url(url)		


sleep(120)
driver.close()

print('Get Gerda data: done')