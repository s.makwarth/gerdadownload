# GerdaDownlaod
The scripts initiates a query in GERDA (the danish national database for 
geophysical data) and downloads the data once the query is succeed. 

## Requirements
Python, Chrome browser, chromedriver for python and Microsoft Outlook

## Launch
* Run GetGerdaData.py to initiate the data query in GERDA.
* Run receive_mail_outlook.py to scan outlook for urls regarding the gerda data.
* Run UnzipAllGerdaData.bat to unzip and clean up the folder, such that the firebird database for each geophysical method remains.

## Author and License
GerdaDownlaod is written by [Simon Makwarth](mailto:simak@mst.dk), and is 
licensed under the GNU Public License version 3 or later. See 
[LICENSE](LICENSE) for details.
